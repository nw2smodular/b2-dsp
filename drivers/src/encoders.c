/*

	nw2s::b2dsp - A microcontroller-based modular synth control framework
	Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*

	This is a char driver that exposes values of the three eQEP units

*/

#include <linux/init.h>           
#include <linux/module.h>         
#include <linux/i2c.h>
#include <linux/device.h>         
#include <linux/kernel.h>         
#include <linux/fs.h>             
#include <linux/mutex.h>	  
#include <linux/stddef.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/ioport.h>
#include <linux/uaccess.h>

#include <asm/io.h>

#include "../include/eqep.h"

#define DEVICE_NAME "encoders"
#define CLASS_NAME  "encoder"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Thomas Scott Wilson");
MODULE_DESCRIPTION("Exposes the three eQEP unit absolute values");
MODULE_VERSION("0.1");

static int major_number;
static struct class* eqepchar_class = NULL;
static struct device* eqepchar_device = NULL;

/* dev IO Mutex */
static DEFINE_MUTEX(eqepchar_mutex);	    

/* Device Prototypes */
static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);
static void logdata(void);

/* File Structure */
static struct file_operations fops =
{
	.open = dev_open,
	.read = dev_read,
	.write = dev_write,
	.release = dev_release,
};

//#define SET_EQEP_REGISTER(x, y, z) *(uint32_t *)(eqep_mapped[x] + y) = z;	

/* Memory access to eqep chips */
static void* eqep_mapped[3];

/* Pointer to position registers */
static volatile uint32_t *position_p[3];

/* Last positions */
static volatile int32_t last_position[3];

/* Address Management for memory access */
static phys_addr_t eqep_base[3] = { ADDR_EQEP0, ADDR_EQEP1, ADDR_EQEP2 };
static char* eqep_name[3] = { NAME_EQEP0, NAME_EQEP1, NAME_EQEP2 };
static struct resource* eqep_region[3];

static int __init eqep_init(void)
{	
	int i;
	uint32_t position_test;
	
	for (i = 0; i < 3; i++)
	{		
		eqep_region[i] = request_mem_region(eqep_base[i], EQEP_BLOCK_LENGTH, eqep_name[i]);
		
		if (eqep_region[i] == NULL)
		{
			printk(KERN_ERR "Could not reserve eQEP %d\n", i);
		}
		else
		{
			printk(KERN_INFO "Successfully reserved eQEP %d\n", i);
		}

		/* Map the memory areas to something we can use */
		eqep_mapped[i] = ioremap(eqep_base[i], EQEP_BLOCK_LENGTH);

		/* Store a pointer directly to the position register */
		position_p[i] = eqep_mapped[i] + EQEP_QPOSCNT;

		/* Set the default values */
		iowrite16(0, eqep_mapped[i] + EQEP_QDECCTL);
		iowrite16(EQEP_QEPCTL_PHEN, eqep_mapped[i] + EQEP_QEPCTL);
		iowrite16(0, eqep_mapped[i] + EQEP_QCAPCTL);
		iowrite32(0, eqep_mapped[i] + EQEP_QPOSCMP);
		iowrite16(0, eqep_mapped[i] + EQEP_QEINT);
		iowrite16(EQEP_INT_ENABLE_ALL, eqep_mapped[i] + EQEP_QCLR);
		iowrite16(EQEP_QEPSTS_COEF | EQEP_QEPSTS_CDEF | EQEP_QEPSTS_FIMF, eqep_mapped[i] + EQEP_QEPSTS);
		iowrite32(-1, eqep_mapped[i] + EQEP_QPOSMAX);

		/* Set up the clock */
		
		// /* Calculate the timer ticks per second */
		// period = 1000000000;
		// period = period * eqep->clk_rate;
		// do_div(period, NSEC_PER_SEC);
		//
		// /* Set this period into the unit timer period register */
		// writel(period, eqep->mmio_base + QUPRD);
		// dev_dbg(&pdev->dev, "QUPRD:0x%08x\n", (u32) period);

		position_test = ioread32(eqep_mapped[i] + EQEP_QPOSCNT);
		
		printk(KERN_INFO "Initialized eQEP %d and read initial value of %d\n", i, position_test);
	}
	
	/* Set up /dev node */
	/* Set up /dev node */
	/* Set up /dev node */
	
	/* Allocate a major number */
	major_number = register_chrdev(0, DEVICE_NAME, &fops);

	if (major_number < 0)
	{
		printk(KERN_ERR "eqep driver failed to register a major number");
		return major_number;
	}

	printk(KERN_INFO  "eqep: registered correctly with major number %d\n", major_number);

	/* Register the device class */
	eqepchar_class = class_create(THIS_MODULE, CLASS_NAME);

	if (IS_ERR(eqepchar_class))
	{
		unregister_chrdev(major_number, DEVICE_NAME);
		printk(KERN_ERR "Failed to register device class\n");
		return PTR_ERR(eqepchar_class);
	}

	printk(KERN_INFO  "eqep: device class registered correctly\n");

	/* Register the device driver */
	eqepchar_device = device_create(eqepchar_class, NULL, MKDEV(major_number, 0), NULL, DEVICE_NAME);

	if (IS_ERR(eqepchar_device))
	{
		class_destroy(eqepchar_class);
		unregister_chrdev(major_number, DEVICE_NAME);
		printk(KERN_ERR "Failed to create the device\n");
		return PTR_ERR(eqepchar_device);
	}

	printk(KERN_INFO  "eqep: device class created correctly\n");

	mutex_init(&eqepchar_mutex);

	return 0;	
}

static void __exit eqep_exit(void)
{	
	int i;
	
	for (i = 0; i < 3; i++)
	{
		iounmap(eqep_mapped[i]);
		release_mem_region(eqep_base[i], EQEP_BLOCK_LENGTH);
	}
	
	mutex_destroy(&eqepchar_mutex);
	device_destroy(eqepchar_class, MKDEV(major_number, 0));
	class_unregister(eqepchar_class);
	class_destroy(eqepchar_class);
	unregister_chrdev(major_number, DEVICE_NAME);
	
	printk(KERN_INFO "Unloaded encoders.");
}

static int dev_open(struct inode *inodep, struct file *filep)
{
	if (!mutex_trylock(&eqepchar_mutex))
	{
		printk(KERN_ERR "eqep: Device in use by another process");
		return -EBUSY;
	}
	
	return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
	uint32_t result;
	uint8_t i;
	int32_t position_value[3];
	
	while (1)
	{
		/* See if the value changed */
		for (i = 0; i < 3; i++)
		{
			position_value[i] = ioread32(eqep_mapped[i] + EQEP_QPOSCNT);
		}
	
		/* Return the value if we have it */
		result = copy_to_user(buffer, position_value, sizeof(position_value));
		
		if (result > 0)
		{
			printk(KERN_ERR "encoders: unable to copy %d bytes to output buffer", result);
		}
		
		
		logdata();
		
		/* If we don't have it and are not blocking, return 0 */
		return sizeof(position_value);
	
		/* Otherwise, take a nap */
	}
}

static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{	
	// char display[2][16];
	// int i = 0;
	// int j = 0;
	//
	// if (!display_initialized)
	// {
	// 	int err = initialize_display();
	// 	if (err < 0)
	// 	{
	// 		/* There was an error, log and be done */
	// 		printk(KERN_ERR "eqep: error initializing display chip: %d\n", err);
	// 		return 0;
	// 	}
	//
	// 	printk(KERN_INFO  "eqep: chip initialized");
	// 	display_initialized = true;
	// }
	//
	// /* limit our input to 32 characters */
	// if (len > 32)
	// {
	// 	len = 32;
	// }
	//
	// if (len < 32)
	// {
	// 	memset(display, 0x20, 32);
	// }
	//
	// /* Copy the buffer */
	// memcpy(display, buffer, len);
	//
	// send_command(0x01);
	//
	// for (i = 0; i < 2; i++)
	// {
	// 	for (j = 0; j < 16; j++)
	// 	{
	//
	// 		// if (previous_frame[i][j] != display[i][j])
	// 		// {
	// 			send_data(display[i][j]);
	// 		// 	previous_frame[i][j] = display[i][j];
	// 		// }
	// 		// else
	// 		// {
	// 		// 	send_command(0x14);
	// 		// }
	// 	}
	//
	// 	send_command(0xA0);
	// }
	//
	// printk(KERN_DEBUG  "Got some data\n");
	// return len;
	
	return 0;
}

static int dev_release(struct inode *inodep, struct file *filep)
{
	//TODO: CLose the i2c driver

	mutex_unlock(&eqepchar_mutex);
	printk(KERN_INFO  "eqep: Device successfully closed\n");
	return 0;
}


module_init(eqep_init);
module_exit(eqep_exit);

static void logdata()
{
	int i;
	
	for (i = 0; i < 3; i++)
	{
		printk("%d EQEP_QPOSCNT   %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSCNT));
		printk("%d EQEP_QPOSINIT  %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSINIT));
		printk("%d EQEP_QPOSMAX   %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSMAX));
		printk("%d EQEP_QPOSCMP   %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSCMP));
		printk("%d EQEP_QPOSILAT  %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSILAT));
		printk("%d EQEP_QPOSSLAT  %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSSLAT));
		printk("%d EQEP_QPOSLAT   %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QPOSLAT));
		printk("%d EQEP_QUTMR     %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QUTMR));
		printk("%d EQEP_QUPRD     %08x\n", i, ioread32(eqep_mapped[i] + EQEP_QUPRD));
		printk("%d EQEP_QWDTMR    %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QWDTMR));
		printk("%d EQEP_QWDPRD    %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QWDPRD));
		printk("%d EQEP_QDECCTL   %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QDECCTL));
		printk("%d EQEP_QEPCTL    %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QEPCTL));
		printk("%d EQEP_QCAPCTL   %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QCAPCTL));
		printk("%d EQEP_QPOSCTL   %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QPOSCTL));
		printk("%d EQEP_QEINT     %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QEINT));
		printk("%d EQEP_QCTMR     %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QCTMR));
		printk("%d EQEP_QCPRD     %04x\n", i, ioread16(eqep_mapped[i] + EQEP_QCPRD));
		printk("%d EQEP_REVID     %08x\n", i, ioread32(eqep_mapped[i] + EQEP_REVID));
	}
}


