/*

	nw2s::b2dsp - A microcontroller-based modular synth control framework
	Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*

	This is an i2c device driver and character devices that provides 
	a write-able node.

	Character writes should be done 32 characters at a time which will
	fill up the character display with every write.

	A sample python script to test this is:

	with open("/dev/nw2s_display", "a") as display:
		display.write("abcdefghijklmnopqrstuvwxyz012345")


	TODO: Flicker when writing the whole screen, even with repeated chars - why is that?

*/

#include <linux/init.h>           
#include <linux/module.h>         
#include <linux/i2c.h>
#include <linux/device.h>         
#include <linux/kernel.h>         
#include <linux/fs.h>             
#include <linux/mutex.h>	  
#include <linux/stddef.h>
#include <linux/slab.h>
#include <linux/delay.h>

#define DISPLAY_SIZE 2*16
#define I2C_ADAPTER_NUMBER 4

/* Static state management */
static bool display_initialized = false;
// static char previous_frame[2][16] = { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
//  								 	  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

#define DEVICE_NAME "nw2s_display"
#define CLASS_NAME  "display"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Thomas Scott Wilson");
MODULE_DESCRIPTION("Drives the i2c-based character display for nw2s::b");
MODULE_VERSION("0.1");

/* Pointer to the actual display chip and attached adapter */
struct i2c_adapter* display_adapter = NULL;
struct i2c_client* display_device = NULL;

static int majorNumber;
static struct class* displaycharClass = NULL;
static struct device* displaycharDevice = NULL;

/* dev IO Mutex */
static DEFINE_MUTEX(displaychar_mutex);	    

/* Display Specifc Prototypes */
static int initialize_display(void);
static int send_command(unsigned char c);
static int send_data(unsigned char c);

/* Device Prototypes */
static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

/* File Structure */
static struct file_operations fops =
{
	.open = dev_open,
	.read = dev_read,
	.write = dev_write,
	.release = dev_release,
};
	 
static const struct i2c_device_id display_id[] = {
	{ "nw2s_display", 0 },
	{ }
};

/* Driver Structure */
static struct i2c_driver display_driver = {

	.driver = {

		.name = "nw2s_display",
	},
	
	.id_table = display_id,
};

/* This is the base chip that we support */
static struct i2c_board_info board = {

	.type = "nw2s_display",
	.flags = 0,
	.addr = 0x3c,
};

static int __init display_init(void)
{
	/* Set up i2c driver */
	int err = i2c_add_driver(&display_driver);
	
	/* Display always connected to bus 2 */
	display_adapter = i2c_get_adapter(I2C_ADAPTER_NUMBER);

	/* Display always has the same name/address */
	display_device = i2c_new_device(display_adapter, &board);
	
	printk(KERN_INFO  "display: registered i2c driver with result %d\n", err);
	
	if (err != 0) return err;

	/* Set up /dev node */
	
	/* Allocate a major number */
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);

	if (majorNumber < 0)
	{
		printk(KERN_ERR "display driver failed to register a major number");
		return majorNumber;
	}

	printk(KERN_INFO  "display: registered correctly with major number %d\n", majorNumber);

	/* Register the device class */
	displaycharClass = class_create(THIS_MODULE, CLASS_NAME);

	if (IS_ERR(displaycharClass))
	{
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ERR "Failed to register device class\n");
		return PTR_ERR(displaycharClass);
	}

	printk(KERN_INFO  "display: device class registered correctly\n");

	/* Register the device driver */
	displaycharDevice = device_create(displaycharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);

	if (IS_ERR(displaycharDevice))
	{
		class_destroy(displaycharClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ERR "Failed to create the device\n");
		return PTR_ERR(displaycharDevice);
	}

	printk(KERN_INFO  "displaychar: device class created correctly\n");
	mutex_init(&displaychar_mutex);
	return 0;
	
}

static void __exit display_exit(void)
{
	i2c_unregister_device(display_device);
	
	i2c_del_driver(&display_driver);

	mutex_destroy(&displaychar_mutex);
	device_destroy(displaycharClass, MKDEV(majorNumber, 0));
	class_unregister(displaycharClass);
	class_destroy(displaycharClass);
	unregister_chrdev(majorNumber, DEVICE_NAME);
}

static int dev_open(struct inode *inodep, struct file *filep)
{
	if (!mutex_trylock(&displaychar_mutex))
	{
		printk(KERN_ERR "displaychar: Device in use by another process");
		return -EBUSY;
	}
	
	return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
	return 0;
}

static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{	
	char display[2][16];
	int i = 0;
	int j = 0;
	
	if (!display_initialized)
	{
		int err = initialize_display();
		if (err < 0)
		{
			/* There was an error, log and be done */
			printk(KERN_ERR "display: error initializing display chip: %d\n", err);
			return 0;
		}

		printk(KERN_INFO  "display: chip initialized");
		display_initialized = true;
	}

	/* limit our input to 32 characters */
	if (len > 32)
	{
		len = 32;
	}

	if (len < 32)
	{
		memset(display, 0x20, 32);
	}
	
	/* Copy the buffer from user space to kernel space */
	copy_from_user(display, buffer, len);

	send_command(0x01); 
	
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 16; j++)
		{
			
			// if (previous_frame[i][j] != display[i][j])
			// {
				send_data(display[i][j]);
			// 	previous_frame[i][j] = display[i][j];
			// }
			// else
			// {
			// 	send_command(0x14);
			// }
		}
		
		send_command(0xA0);
	}

	printk(KERN_DEBUG  "Got some data\n");
	return len;
}

static int dev_release(struct inode *inodep, struct file *filep)
{
	//TODO: CLose the i2c driver

	mutex_unlock(&displaychar_mutex);
	printk(KERN_INFO  "display: Device successfully closed\n");
	return 0;
}


module_init(display_init);
module_exit(display_exit);


static int initialize_display(void)
{
	send_command(0x2A);  //function set (extended send_command set)
	send_command(0x71);  //function selection A, disable internal Vdd regualtor
	send_data(0x00);
	send_command(0x28);  //function set (fundamental send_command set)
	send_command(0x08);  //display off, cursor off, blink off
	send_command(0x2A);  //function set (extended send_command set)
	send_command(0x79);  //OLED send_command set enabled
	send_command(0xD5);  //set display clock divide ratio/oscillator frequency
	send_command(0x70);  //set display clock divide ratio/oscillator frequency
	send_command(0x78);  //OLED send_command set disabled
	send_command(0x09);  //extended function set (4-lines)
	send_command(0x06);  //COM SEG direction
	send_command(0x72);  //function selection B, disable internal Vdd regualtor
	send_data(0x00);     //ROM CGRAM selection
	send_command(0x2A);  //function set (extended send_command set)
	send_command(0x79);  //OLED send_command set enabled
	send_command(0xDA);  //set SEG pins hardware configuration
	send_command(0x10);  //set SEG pins ... NOTE: When using NHD-0216AW-XB3 or NHD_0216MW_XB3 change to (0x00)
	send_command(0xDC);  //function selection C
	send_command(0x00);  //function selection C
	send_command(0x81);  //set contrast control
	send_command(0x7F);  //set contrast control
	send_command(0xD9);  //set phase length
	send_command(0xF1);  //set phase length
	send_command(0xDB);  //set VCOMH deselect level
	send_command(0x40);  //set VCOMH deselect level
	send_command(0x78);  //OLED send_command set disabled
	send_command(0x28);  //function set (fundamental send_command set)
	send_command(0x01);  //clear display
	send_command(0x80);  //set DDRAM address to 0x00
	send_command(0x0C);  //display ON
	
	msleep(100);
		
	return 0;	
}

static int send_command(unsigned char c)
{
	unsigned char packet[2];
	
	packet[0] = 0x00;
	packet[1] = c;
	
	return i2c_master_send(display_device, packet, 2);
}

static int send_data(unsigned char c)
{
	unsigned char packet[2];
	
	packet[0] = 0x40;
	packet[1] = c;
	
	return i2c_master_send(display_device, packet, 2);
}



