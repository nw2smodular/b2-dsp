# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import struct
import array

from sets import Set
from fcntl import ioctl

from models import gamepadmodel


#TODO: refresh sometimes
_device_names = Set()
_devices = { }


def initialize():

    refresh()


def refresh():

    # Refresh the device list every once in a while
    for device_filename in os.listdir('/dev/input'):

        if device_filename.startswith('js') and device_filename not in _device_names:

            # TODO: We can write to these guys too
            # Open the file, blocking
            f = open("/dev/input/" + device_filename, 'rb')
            
            # Get the device name JSIOCGNAME(len)
            buf = array.array('c', ['\0'] * 64)
            ioctl(f, 0x80006a13 + (0x10000 * len(buf)), buf)
            name = buf.tostring()
            
            # TODO - just log it
            print(name)
            
            # Add to the device list
            _devices[f.fileno()] = {
                
                "source" : gamepadmodel.SOURCE_GAMEPAD,
                "name" : name,
                "file" : f,
                "decode_chunk" : decode_chunk,
                "chunk_size" : 8
            }

            # TODO: Need to remove them too
            _device_names.add(device_filename)


def get_devices():
    
    return _devices


def decode_chunk(chunk):
    
    time, value, type, number = struct.unpack('IhBB', chunk)

    return { "type" : type, "number" : number, "value" : value }


