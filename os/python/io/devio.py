# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This provides some tools to do blocking or non-blocking polling of
# devices that are generally found in /dev/input like keyboards, 
# mice, game pads, etc. Those who need the input can poll and decide 
# if anything they get back is what they need.
#
# Generally the only thing that should be reading is the interface worker
# but once we get to some other modules, we may have 

from io import gameio

from select import select


_handlers = { }
_devices = { }

def initialize():

    refresh()
    

def refresh():

    gameio.refresh()

    _devices.update(gameio.get_devices())

    
def add_handler(name, filter, handler):

    _handlers[name] = { "filter" : filter, "handler" : handler }
    

def process():
    
    while 1:

        # TODO Monitor udev for new devices instead?
        # Wait for a device to have data ready, abort after 5 seconds and refresh the device list
        r, w, x = select(_devices.keys(), [], [], 5)

        for fd in r:

            # Get a handier reference to the device that has data ready
            device = _devices[fd]

            # Decode the available chunks
            event = device["decode_chunk"](device["file"].read(device["chunk_size"]))

            # Filter and handle the chunks
            for handler in _handlers.values():

                if handler["filter"](device, event): handler["handler"](device, event)

        # After select times out, refresh the devices
        refresh()
