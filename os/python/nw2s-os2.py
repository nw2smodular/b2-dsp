# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This is the main program for the nw2s::b2/dsp operating system. 
# The main thing it does is start up a few worker threads that manage
# the display output, the menu interaction, and any controller inputs
# as necessary

from multiprocessing import Process

from workers import displayworker
from workers import interfaceworker


# Start up the display worker
display_process = Process(target=displayworker.run)
display_process.start()

# Start up the interface worker
interface_process = Process(target=interfaceworker.run)
interface_process.start()


# And wait for all the threads to finish
display_process.join()
interface_process.join()