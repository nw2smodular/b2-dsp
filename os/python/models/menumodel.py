# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from services import systemservice


menustructure = {
    
    "text" : "main menu",
    
    "items" : [ 
        {
            "text" : "load patch"
        },    
        {
            "text" : "load user"
        },    
        {
            "text" : "preferences"
        },    
        {
            "text" : "network info",
        
            "items" : [
            
                {
                       "text" : "hostname",
                       "itemcommand" : systemservice.gethostname
                },
                {
                       "text" : "ip address",
                       "itemcommand" : systemservice.getipaddress
                },
                {
                       "text" : "default login"
                }
            ]
        },    
        {
            "text" : "system info"
        }      
    ]
}


