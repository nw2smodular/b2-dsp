# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ctypes

from multiprocessing import Lock
from multiprocessing.sharedctypes import Array
from multiprocessing.sharedctypes import Value


_lock = Lock()
_model_modified = Value(ctypes.c_bool, True, lock=_lock)
_line_one = Array('c', '                ', lock=_lock)
_line_two = Array('c', '                ', lock=_lock)

def display_line_one(text):
    
    _model_modified.value = True
    _line_one.value = '{0: <16}'.format(text[:16])

    
def display_line_two(text):
    
    _model_modified.value = True
    _line_two.value = '{0: <16}'.format(text[:16])

def is_modified():
    
    return _model_modified.value
    
def get_data():
    
    data = _line_one.value + _line_two.value
    
    _model_modified.value = False
    
    return data




