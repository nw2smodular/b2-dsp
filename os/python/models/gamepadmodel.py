# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Source ID for Gamepad
SOURCE_GAMEPAD = 0x01

# Types
TYPE_BUTTON = 0x01
TYPE_AXIS = 0x02


# PS3 Buttons

DPAD_UP = 4
DPAD_RIGHT = 5
DPAD_DOWN = 6
DPAD_LEFT = 7
BUTTON_X = 14
BUTTON_CIRCLE = 13
BUTTON_TRIANGLE = 12
BUTTON_SQUARE = 15
BUTTON_SELECT = 0
BUTTON_START = 3
BUTTON_HOME = 16
BUTTON_LHAT = 1
BUTTON_RHAT = 2
BUTTON_L1 = 10
BUTTON_L2 = 8
BUTTON_R1 = 11
BUTTON_R2 = 9



