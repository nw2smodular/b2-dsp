# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import socket
import fcntl
import struct

from models import systemmodel

def gethostname():
    
    return socket.gethostname()
    

def getipaddress():

    ifname = systemmodel.INTERFACE_NAME

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # SIOCGIFADDR
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915,  struct.pack('256s', ifname[:15]))[20:24])