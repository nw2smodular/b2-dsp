
# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This is the worker thread for the OLED display. It opens up a stream to 
# the display driver and updates whenever there are changes, but no more
# often than about 20 times a second to save processing


import time
import evdev

from io import devio
from models import displaymodel
from models import gamepadmodel
from models.menumodel import menustructure

#from controllers import loadercontroller

menu_node = menustructure
current_stack = [menustructure, None, None, None]
current_index = [0, 0, 0, 0]
current_level = 0

#menu_level1_index = 0
#menu_level1_count = len(menustructure)


#TODO: Filter on mfr and device
def filter_buttons(device, event):
        
    # These are the gamepad buttons we are interested in for menu navigation
    return (device["source"] == gamepadmodel.SOURCE_GAMEPAD and 
            event["type"] == gamepadmodel.TYPE_BUTTON and 
            event["number"] in [gamepadmodel.DPAD_LEFT, gamepadmodel.DPAD_RIGHT, gamepadmodel.BUTTON_X, gamepadmodel.BUTTON_CIRCLE] and
            event["value"] == 1)


def handle_buttons(device, event):
    
    # This code performs the actual menu navigation until we get to a command that has it's own handlers
    global menu_node
    global current_level
    
    changed = False
    
    # Move to the right in the menu    
    if event["number"] == gamepadmodel.DPAD_LEFT:
        
        current_index[current_level] = (current_index[current_level] - 1) % len(menu_node["items"])
        
        # TODO: not necessarily
        changed = True

    # Move to the left in the menu
    elif event["number"] == gamepadmodel.DPAD_RIGHT:
        
        current_index[current_level] = (current_index[current_level] + 1) % len(menu_node["items"])
        
        # TODO: not necessarily
        changed = True

    # Run a command to give us the next single item
    elif event["number"] == gamepadmodel.BUTTON_X and "itemcommand" in menu_node["items"][current_index[current_level]]:
    
        # Remember where we are in the three for navigating back up later
        current_stack[current_level + 1] = menu_node

        # Run the command and get the next layer item, turn it into a list, generally for information only
        menu_node["items"][current_index[current_level]]["items"] = [ { "text" : menu_node["items"][current_index[current_level]]["itemcommand"]() } ]

        # Make our node pointer point to the selected menu item
        menu_node = menu_node["items"][current_index[current_level]]
        
        # Increment the current level by 1
        current_level += 1
        
        # Reset the current index for our new level to zero
        current_index[current_level] = 0
    
        changed = True
        

    # Move to the next lower level of menu
    elif event["number"] == gamepadmodel.BUTTON_X and "items" in menu_node["items"][current_index[current_level]] and len(menu_node["items"][current_index[current_level]]["items"]) > 0:

        # Remember where we are in the three for navigating back up later
        current_stack[current_level + 1] = menu_node
        
        # Make our node pointer point to the selected menu item
        menu_node = menu_node["items"][current_index[current_level]]
        
        # Increment the current level by 1
        current_level += 1
        
        # Reset the current index for our new level to zero
        current_index[current_level] = 0
    
        changed = True

    elif event["number"] == gamepadmodel.BUTTON_CIRCLE and current_level > 0:
        
        # Make our node pointer point to the selected menu item
        menu_node = current_stack[current_level]

        current_level -= 1
        
        changed = True


    # Only update if a change was made - to avoid flicker
    if changed:

        displaymodel.display_line_one(menu_node["text"])
        displaymodel.display_line_two(menu_node["items"][current_index[current_level]]["text"])
    

def run():

    # Show welcome message and sleep for 1s
    displaymodel.display_line_one("nw2s:b2/dsp v0.1")
    
    time.sleep(1)
    
    # TODO: Store the last patch and reload when booting, then show that display

    # Show the main menu: 
    displaymodel.display_line_one(menu_node["text"])
    displaymodel.display_line_two(menu_node["items"][current_index[current_level]]["text"])
    
    # Initialize our possible input devices
    devio.initialize()
    
    # Set up our filters and callbacks
    devio.add_handler("interface_game_buttons", filter_buttons, handle_buttons)
    
    # Hand off control to the input manager
    devio.process()
    
    
