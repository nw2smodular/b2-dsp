
# nw2s::b2dsp - A microcontroller-based modular synth control framework
# Copyright (C) 2016 Scott Wilson (thomas.scott.wilson@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This is the worker thread for the OLED display. It opens up a stream to 
# the display driver and updates whenever there are changes, but no more
# often than about 20 times a second to save processing

import time

from models import displaymodel


def run():

    display_device = open("/dev/nw2s_display", "a")

    while 1:

        if displaymodel.is_modified():

            display_device.write(displaymodel.get_data())
        
            display_device.flush()
        
        time.sleep(0.05)


    display_device.close()